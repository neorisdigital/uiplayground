import { Component } from '@angular/core';
import {TranslationService} from '../../../uicomponents/services/translation.service';
import {Logger} from '../../../uicomponents/services/logger.service';
import {JobsiteService} from '../../../uicomponents/services/jobsite.service';
import {SessionService} from '../../../uicomponents/services/session.service';
import {UserDTO} from '../../../uicomponents/DTO/userDTO';
import {OrderService} from '../../../uicomponents/services/order.service';
import orderDTO from '../../../uicomponents/DTO/orderDTO';
import {Broadcaster} from '../../../uicomponents/events/broadcaster.event';
import {SidebarComponent} from '../../../uicomponents/components/sidebar/sidebar.component';
import { orderCardComponent } from '../../../uicomponents/components/ordercard/ordercard.component';

import 'rxjs/add/operator/toPromise';


@Component({
    selector: 'client-detail-component',
    templateUrl: './client.detail.component.html',
    styleUrls: ['./client.detail.component.scss'],
    providers: [TranslationService, JobsiteService, OrderService, Broadcaster, SidebarComponent]
})
export class ClientDetailComponent {

    public userm: UserDTO = new UserDTO();
    public progressBarData: any[] = [];
    public halfDonutData: any[] = [];
    public ordersInCache: any = [];

    public firstNumber: number = 0;
    public secondNumber: number = 0;
    public thirdNumber: number = 0;


    constructor(private t: TranslationService, public jobsiteService: JobsiteService,
        private session: SessionService, public orderService: OrderService, private eventDispatcher: Broadcaster, private sideBarComponent: SidebarComponent) {

        this.eventDispatcher.on<string>(SidebarComponent.JOBSITE_SELECTED)
            .subscribe(response => {
                //Logger.log("--event-chart->:"+response);
                this.orderService.getOrders(Number(response)).subscribe(
                    res => {
                        this.ordersInCache = res.Orders;
                        this.chartDataProviders(res.Orders);
                    },
                    err => { console.error('Error', err) }
                )
            });

    }

    public shouldShowOrder(): orderDTO {
        return orderCardComponent.lastSelectedOrder;
    }

    public chartDataProviders(data: any): void {
        let progressBar: any = [{ title: this.t.pt("views.progress-chart.active") + " 0 / 0", volume: 0, progress: 0, color: "#8BC34A", total: 0 }
            , { title: this.t.pt("views.progress-chart.inprogress") + " 0 / 0", volume: 0, progress: 0, color: "#3EC0FA", total: 0 }
            , { title: this.t.pt("views.progress-chart.tobeconfirmed") + " 0 / 0", volume: 0, progress: 0, color: "#EC407A", total: 0 }
            , { title: this.t.pt("views.progress-chart.onhold") + " 0 / 0", volume: 0, progress: 0, color: "#FFC82C", total: 0 }
            , { title: this.t.pt("views.progress-chart.blocked") + " 0 / 0", volume: 0, progress: 0, color: "#585858", total: 0 }
            , { title: this.t.pt("views.progress-chart.completed") + " 0 / 0", volume: 0, progress: 0, color: "#9013FE", total: 0 }];
        let dispatchdVolume: any = [{ value: 0, label: this.t.pt("views.half-chart.dispatchedvolume") },
            { value: 0, label: this.t.pt("views.half-chart.totalvolume") }];
        for (let item of data) {
            if (item.orderStatusGroupCode == "CNC" || item.orderStatusGroupCode == "CMP") {
                progressBar[5].total = progressBar[5].total + 1;
            }
            if (item.orderStatusGroupCode == "OHD") {

                progressBar[3].total = progressBar[3].total + 1;

            }
            if (item.orderStatusGroupCode == "NST") {

                progressBar[0].total = progressBar[0].total + 1;

            }
            if (item.orderStatusGroupCode == "STR") {

                progressBar[1].total = progressBar[1].total + 1;
            }
            if (item.orderStatusGroupCode == "WCL") {

                progressBar[2].total = progressBar[2].total + 1;

            }
            if (item.orderStatusGroupCode == "PHD") {

                progressBar[4].total = progressBar[4].total + 1;

            }
            //check every volume
            for (let load of item.orderItem) {
                if (item.orderStatusGroupCode == "CNC" || item.orderStatusGroupCode == "CMP") {
                    progressBar[5].volume = progressBar[5].volume + Number(load.productQuantity);
                    progressBar[5].progress = Math.ceil((progressBar[5].total * 100) / data.length);
                    progressBar[5].title = this.t.pt("views.progress-chart.completed") + " " + progressBar[5].total + " / " + progressBar[5].volume;
                }
                if (item.orderStatusGroupCode == "OHD") {
                    progressBar[3].volume = progressBar[3].volume + Number(load.productQuantity);
                    progressBar[3].progress = Math.ceil((progressBar[3].total * 100) / data.length);
                    progressBar[3].title = this.t.pt("views.progress-chart.onhold") + " " + progressBar[3].total + " / " + progressBar[3].volume;
                }
                if (item.orderStatusGroupCode == "NST") {
                    progressBar[0].volume = progressBar[0].volume + Number(load.productQuantity);
                    progressBar[0].progress = Math.ceil((progressBar[0].total * 100) / data.length);
                    progressBar[0].title = this.t.pt("views.progress-chart.active") + " " + progressBar[0].total + " / " + progressBar[0].volume;
                }
                if (item.orderStatusGroupCode == "STR") {
                    progressBar[1].volume = progressBar[1].volume + Number(load.productQuantity);
                    progressBar[1].progress = Math.ceil((progressBar[1].total * 100) / data.length);
                    progressBar[1].title = this.t.pt("views.progress-chart.inprogress") + " " + progressBar[1].total + " / " + progressBar[1].volume;
                }
                if (item.orderStatusGroupCode == "WCL") {
                    progressBar[2].volume = progressBar[2].volume + Number(load.productQuantity);
                    progressBar[2].progress = Math.ceil((progressBar[2].total * 100) / data.length);
                    progressBar[2].title = this.t.pt("views.progress-chart.tobeconfirmed") + " " + progressBar[1].total + " / " + progressBar[1].volume;
                }
                if (item.orderStatusGroupCode == "PHD") {
                    progressBar[4].volume = progressBar[4].volume + Number(load.productQuantity);
                    progressBar[4].progress = Math.ceil((progressBar[4].total * 100) / data.length);
                    progressBar[4].title = this.t.pt("views.progress-chart.blocked") + " " + progressBar[4].total + " / " + progressBar[4].volume;
                }
                if (load.orderDetStatusGroupCode == "CMP") {
                    dispatchdVolume[0].value = dispatchdVolume[0].value + load.productQuantity;
                }
                dispatchdVolume[1].value = dispatchdVolume[1].value + load.productQuantity;
            }
            this.firstNumber = dispatchdVolume[0].value;
            this.secondNumber = Math.ceil((dispatchdVolume[0].value * 100) / dispatchdVolume[1].value);
            this.thirdNumber = dispatchdVolume[1].value
        }

        this.halfDonutData = dispatchdVolume;
        this.progressBarData = progressBar;

        //Logger.log(this.progressBarData);
    }

    public buttonClicked(): void {
        //console.log("---")
        let varia: any = this.jobsiteService.getAllJobsites();
    }
}