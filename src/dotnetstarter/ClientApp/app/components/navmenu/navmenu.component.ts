import { Component } from '@angular/core';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})
export class NavMenuComponent {
    public components_list:any[] = [
             { name: "Cemex Button", href: "/cemex-button" },
             { name: "Cemex Loader", href: "/cemex-loader" },
             { name: "Cemex Modal", href:"/cemex-modal" },
             { name: "Cemex Progress Chart", href: "/cemex-progress-chart" },
             { name: "Cemex Simple Card", href: "/cemex-simple-card" },
             { name: "Cemex Table", href: "/table" },
             { name: "Cemex Chart Donut", href: "/cemex-chart-donut" },
             { name: "Cemex Login", href: "/login" },
             { name: "Cemex Half Donut", href: "/cemex-half-donut" },
             { name: "Translation", href: "/translation" },
             { name: "Pod Filter", href: "/pod-filter" },
             { name: "Filter", href: "/filter" },
             { name: "Calendar", href: "/calendar" },
             { name: "Jobsite Header", href: "/jobsite-header" },
             { name: "Order Card", href: "/order-card" },
             { name: "Side Bar", href: "/side-bar" },
             { name: "Cemex Session Bar", href: "/cemex-session-bar" },
             { name: "Chart Gauge", href: "/gauge-example"},
             { name: "Detail Cliente" , href:"/client-detail"},
             { name: "Counter" , href:"/counter-example"},
             { name: "Delivery Info" , href:"/delivery-info-example"},
             { name: "Delivery Template" , href:"/delivery-template-example"},
             { name: "Typehead" , href:"/typehead-example"},
             { name: "Status Filter" , href:"/status-filter-example"},

             { name: "Md Radio Button" , href:"/demo-md-radio-button"},
             { name: "Md Icon" , href:"/demo-md-icon"},
             { name: "Md Input Container" , href:"/demo-md-input-container"},
             { name: "Md Select Container" , href:"/demo-md-select-container"},
             { name: "Md Button" , href:"/demo-md-button"}



    ];
    public search_component:string = "";
}