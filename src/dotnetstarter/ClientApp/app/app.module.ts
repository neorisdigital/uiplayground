import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { AppComponent } from './components/app/app.component'
import { HomeComponent } from './components/home/home.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HttpCemex } from '../uicomponents/services/http.service';
import { ApiService } from './../uicomponents/services/http.api.service';
import { AuthGuard } from '../uicomponents/services/auth-guard.service';
import { Broadcaster } from '../uicomponents/events/broadcaster.event';
import { SessionService } from '../uicomponents/services/session.service';
import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';


//UI Components 
import { CemexButtonComponent } from '../uicomponents/components/cemex-button/cemex-button.component';
import { CemexLoaderComponent } from '../uicomponents/components/cemex-loader/cemex-loader.component';
import { CemexModalComponent } from '../uicomponents/components/cemex-modal/cemex-modal.component';
import { JobsiteHeaderComponent } from '../uicomponents/components/jobsite-header/jobsite-header.component';

import { CemexChartDonutComponent } from '../uicomponents/components/cemex-chart-donut/cemex-chart-donut.component';
import { CemexProgressChartComponent } from '../uicomponents/components/cemex-progress-chart/cemex-progress-chart.component';
import { CemexSimpleCardComponent } from '../uicomponents/components/cemex-simple-card/cemex-simple-card.component';
import { TableComponent } from '../uicomponents/components/table/table.component';
import { SidebarComponent } from '../uicomponents/components/sidebar/sidebar.component';
import { HistoricalFilterComponent}     from '../uicomponents/components/historical-filter/historical-filter.component';


import { FetchDataComponent } from '../uicomponents/components/fetchdata/fetchdata.component';
import { LoginComponent } from '../uicomponents/components/login/login.component';
import { CemexHalfDonutComponent } from '../uicomponents/components/cemex-half-donut/cemex-half-donut.component';
import { TypeaheadComponent } from '../uicomponents/components/typeahead/typeahead.component';
import { TranslationComponent } from '../uicomponents/components/translation/translation.component';
import { FilterComponent } from '../uicomponents/components/filter/filter.component'
import { PodFilterComponent } from '../uicomponents/components/pod-filter/pod-filter.component';
import { CalendarComponent } from '../uicomponents/components/calendar/calendar.component';
import { orderCardComponent } from '../uicomponents/components/ordercard/ordercard.component';
import { StatusFilterComponent } from '../uicomponents/components/status-filter/status-filter.component';
import { OrderTicketHeaderComponent } from '../uicomponents/components/order-ticket-header/order-ticket-header.component';
import { JobsiteFilterComponent } from '../uicomponents/components/jobsite-filter/jobsite-filter.component';
import { CemexSessionBarComponent} from '../uicomponents/components/cemex-session-bar/cemex-session-bar.component';
import { DoughnutChartDemoComponent} from '../uicomponents/components/chart-gauge/chart-gauge.component';
import { CounterComponent } from '../uicomponents/components/counter/counter.component';
import { DeliveryInfoComponent } from '../uicomponents/components/delivery-info/delivery-info.component';
import { DeliveryTemplateComponent } from '../uicomponents/components/delivery-template/delivery-template.component';

import { MdIconModule } from './../uicomponents/components/md-icon/md-icon.module';
import { MdInputContainerComponent } from '../uicomponents/components/md-input-container/md-input-container.component';
import { MdRadioButtonComponent } from '../uicomponents/components/md-radio-button/md-radio-button.component';
import { MdSelectContainerComponent } from '../uicomponents/components/md-select-container/md-select-container.component';
import { MdButton } from '../uicomponents/components/md-button/md-button.component';


//Pipes 
import { FilterByPipe } from '../uicomponents/pipes/filterby.pipe';
import { OrderByPipe } from '../uicomponents/pipes/orderby.pipe';
import { UniquePipe } from '../uicomponents/pipes/unique.pipe';

//Demo Components
import { DemoCemexButtonComponent } from '../uicomponents/components/cemex-button/demo.component';
import { DemoCemexLoaderComponent } from '../uicomponents/components/cemex-loader/demo.component';
import { DemoCemexChartDonutComponent } from '../uicomponents/components/cemex-chart-donut/demo.component';
import { DemoCemexModalComponent } from '../uicomponents/components/cemex-modal/demo.component';
import { DemoCemexProgressChartComponent } from '../uicomponents/components/cemex-progress-chart/demo.component';
import { DemoCemexSimpleCardComponent } from '../uicomponents/components/cemex-simple-card/demo.component';
import { DemoTableComponent } from '../uicomponents/components/table/demo.component';
import { DemoCemexHalfDonutComponent } from '../uicomponents/components/cemex-half-donut/demo.component';
import { DemoTypeaheadComponent } from '../uicomponents/components/typeahead/demo.component';
import { DateRangePickerDirective } from '../uicomponents/components/ng2-daterangepicker';
import { DemoTranslationComponent } from '../uicomponents/components/translation/demo.component';
import { DemoPodFilterComponent } from '../uicomponents/components/pod-filter/demo.component';
import { DemoFilterComponent } from '../uicomponents/components/filter/demo.component';
import { DemoCalendarComponent } from '../uicomponents/components/calendar/demo.component';
import { DemoJobsiteHeaderComponent } from '../uicomponents/components/jobsite-header/demo.component';
import { DemoOrderCardComponent } from '../uicomponents/components/ordercard/demo.component';
import { DemoSideBarComponent } from '../uicomponents/components/sidebar/demo.component';
import { DemoOrderTicketHeaderComponent } from '../uicomponents/components/order-ticket-header/demo.component';
import { DemoCemexSessionBarComponent } from '../uicomponents/components/cemex-session-bar/demo.component';
import { DemoDoughnutChartDemoComponent } from '../uicomponents/components/chart-gauge/demo.component';
import { DemoCounterComponent } from '../uicomponents/components/counter/demo.component';
import { DemoDeliveryInfoComponent } from '../uicomponents/components/delivery-info/demo.component';
import { DemoDeliveryTemplateComponent } from '../uicomponents/components/delivery-template/demo.component';
import { ClientDetailComponent } from './components/client-detail/client.detail.component';
import { DemoStatusFilterComponent } from '../uicomponents/components/status-filter/demo.component';

import { DemoMdRadioButtonComponent } from '../uicomponents/components/md-radio-button/demo.component';
import { DemoMdInputContainerComponent } from '../uicomponents/components/md-input-container/demo.component';
import { DemoMdIconComponent } from '../uicomponents/components/md-icon/demo.component';
import { DemoMdSelectContainerComponent } from '../uicomponents/components/md-select-container/demo.component';
import { DemoMdButtonComponent } from '../uicomponents/components/md-button/demo.component';


import { Ng2MapModule}                  from 'ng2-map';
import * as _                           from '@types/underscore';

//events
import { SidebarEvents }                from '../uicomponents/events/sidebar.event';
import { CalendarEvents }               from '../uicomponents/events/calendar.event';

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        HomeComponent,
        NavMenuComponent,

        //UI Components 
        CemexButtonComponent,
        CounterComponent,
        DeliveryTemplateComponent,
        TableComponent,
        FetchDataComponent,
        LoginComponent,
        CemexLoaderComponent,
        CemexModalComponent,
        CemexProgressChartComponent,
        JobsiteHeaderComponent,
        DeliveryInfoComponent,
        DateRangePickerDirective,
        CemexSimpleCardComponent,
        CemexChartDonutComponent,
        CemexHalfDonutComponent,
        FilterComponent,
        PodFilterComponent,
        TranslationComponent,
        TypeaheadComponent,
        CalendarComponent,
        orderCardComponent,
        SidebarComponent,
        StatusFilterComponent,
        OrderTicketHeaderComponent,
        JobsiteFilterComponent,
        CemexSessionBarComponent,
        DoughnutChartDemoComponent,
        CounterComponent,
        DeliveryInfoComponent,
        DeliveryTemplateComponent,
        HistoricalFilterComponent,

        
        MdInputContainerComponent,
        MdRadioButtonComponent,
        MdSelectContainerComponent,
        MdButton,
        //Pipes
        FilterByPipe,
        OrderByPipe,
        UniquePipe,

        //Demo Components
        DemoCemexLoaderComponent,
        DemoCemexButtonComponent,
        DemoCemexChartDonutComponent,
        DemoCemexModalComponent,
        DemoTableComponent,
        DemoCemexProgressChartComponent,
        DemoCemexSimpleCardComponent,
        DemoCemexHalfDonutComponent,
        DemoFilterComponent,
        DemoPodFilterComponent,
        DemoTranslationComponent,
        DemoTypeaheadComponent,
        DemoCalendarComponent,
        DemoJobsiteHeaderComponent,
        DemoOrderCardComponent,
        DemoSideBarComponent,
        DemoOrderTicketHeaderComponent,
        DemoCemexSessionBarComponent,
        DemoDoughnutChartDemoComponent,
        DemoCounterComponent,
        DemoDeliveryInfoComponent,
        DemoDeliveryTemplateComponent,
        DemoStatusFilterComponent,

        DemoMdRadioButtonComponent,
        DemoMdIconComponent,
        DemoMdInputContainerComponent,
        DemoMdSelectContainerComponent,
        DemoMdButtonComponent,

        ClientDetailComponent
    ],
    imports: [
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        ChartsModule,
        FormsModule,
        Ng2MapModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'playground', component: DeliveryTemplateComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: 'cemex-button', component: DemoCemexButtonComponent },
            { path: 'cemex-loader', component: DemoCemexLoaderComponent },
            { path: 'cemex-modal', component: DemoCemexModalComponent },
            { path: 'cemex-progress-chart', component: DemoCemexProgressChartComponent },
            { path: 'cemex-simple-card', component: DemoCemexSimpleCardComponent },
            { path: 'cemex-session-bar', component: DemoCemexSessionBarComponent},
            { path: 'table', component: DemoTableComponent },
            { path: 'login', component: LoginComponent },
            { path: 'cemex-chart-donut', component: DemoCemexChartDonutComponent },
            { path: 'cemex-half-donut', component: DemoCemexHalfDonutComponent },
            { path: 'filter', component: DemoFilterComponent },
            { path: 'pod-filter', component: DemoPodFilterComponent },
            { path: 'translation', component: DemoTranslationComponent },
            { path: 'calendar', component: DemoCalendarComponent },
            { path: 'jobsite-header', component: DemoJobsiteHeaderComponent },
            { path: 'order-card', component: DemoOrderCardComponent },
            { path: 'side-bar', component: DemoSideBarComponent },
            { path: 'client-detail', component: ClientDetailComponent },
            { path: 'gauge-example', component: DemoDoughnutChartDemoComponent},
            { path: 'counter-example', component: DemoCounterComponent},
            { path: 'delivery-info-example', component: DemoDeliveryInfoComponent},
            { path: 'delivery-template-example', component: DemoDeliveryTemplateComponent},
            { path: 'typehead-example', component: DemoTypeaheadComponent},
            { path: 'status-filter-example', component: DemoStatusFilterComponent },

            { path: 'demo-md-radio-button', component: DemoMdRadioButtonComponent},
            { path: 'demo-md-icon', component: DemoMdIconComponent},
            { path: 'demo-md-input-container', component: DemoMdInputContainerComponent},
            { path: 'demo-md-select-container', component: DemoMdSelectContainerComponent},
            { path: 'demo-md-button', component: DemoMdButtonComponent},

            { path: '**', redirectTo: 'home' }
        ]), FormsModule, ReactiveFormsModule ,  MdIconModule.forRoot(),
    ],
    providers: [
        {
            provide: ApiService,
            useFactory: (backend: XHRBackend, options: RequestOptions) => {
                return new ApiService(backend, options);
            },
            deps: [XHRBackend, RequestOptions]
        },
        AuthGuard,
        Broadcaster,
        HttpCemex,
        SessionService,
        SidebarEvents,
        CalendarEvents,
    ],
})
export class AppModule {
}