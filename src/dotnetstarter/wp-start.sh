YEL='\033[1;33m'
NC='\033[0m'
RED='\033[1;31m'
GREEN='\033[1;32m'
CY='\033[0;36m'
PR='\033[1;35m'






dotnet restore
echo "${PR} INSTALLING NPM DEPENDENCIES ..... ${NC}" 
npm install
echo "${GREEN} NPM INSTALL DONE ${NC}"
echo "${RED}#####                     (33%)\r ${NC}"
sleep 1
webpack --config webpack.config.vendor.js
echo "${PR}#############             (66%)\r ${NC}"
sleep 1
webpack --config webpack.config.js
export ASPNETCORE_ENVIRONMENT=Development

echo "${GREEN} VENDOR PACKAGES OK ${NC}" 
echo "${GREEN}#######################   (100%)\r ${NC}"
echo '\n'
/usr/bin/open -a "/Applications/Google Chrome.app" 'http://localhost:5000'
echo "when the page is load wait few seconds to project start...." 
echo "${GREEN}have ${cy}fun${NC}" 
dotnet run